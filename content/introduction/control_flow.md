---
title: "Control Flow"
date: 2020-01-13T21:03:44Z
weight: 10
---

Kima features minimal built-in control flow. Other, more complex control flow
constructs can be created using the [[Effects]] feature

## If

```plain
let x: Int = 5;

if (x < 10) {
    print("x is less than 10");
}

if (x == 5) {
    print("x=5");
} else if (x == 6) {
    print("x=6");
} else {
    print("x is not 5 or 6");
}
```
If in Kima functions as you would expect. Give an boolean expression and if it
evaluates to true then the body executes. If an else branch is given, then it is
executed if the condition is false.

The braces around the statements are not required. They are only there for
grouping. If the body only consists of a single statement then you can skip
them. For example, the two statements below are equivalent:

```plain
if (1 < 2) {
    print("1 < 2");
} else {
    print("1 > 2");
}

if (1 < 2)
    print("1 < 2");
else
    print("1 > 2");
```
Now, since an if is also a statement, we can use this to chain them into
else-ifs.

```plain
if (4 > 5) print("4 > 5");
else if (6 > 5) print("4 <=5 and 6 > 5");
else print("Neither is true")
```
## While
While statements, again are no surprise. The boolean expression is evaluated
when the while loop is first encountered, if it is true the body executes, then
we re-evaluate the expression, and if it is true, repeat.

```plain
while (true) {
    print("Printing forever");
}
```
Similarly to an if, we can skip the braces if the loop contains only a single
statement

```plain
while (true) print("Printing forever");
```
