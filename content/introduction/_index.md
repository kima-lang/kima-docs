+++
title = "Introduction"
date = 2020-01-12T21:38:34Z
weight = 10
chapter = true
+++

# Introduction

This document should give you an initial introduction to the Kima language and
help you familiarise yourself with it. It contains plenty of code examples as
well as explanations of them.
