---
title: "Functions"
date: 2020-01-13T21:04:06Z
weight: 20
---

You can define functions at the top level like so:

```plain
fun f(x: Int, y: String) -> Int {
    # code
    5;
}
```
The value returned by the last statement in the function body is returned by the
function.

Functions in Kima are **first-class objects**. This means they can be treated just
like any other value; assigned to variables, passed into other functions or
returned from functions.

You can define functions just like any other value as well.

```plain
let f = fun(x: Int) -> Int { x + 1; }
```
For a function defined locally like this, Kima can infer the return type so you
can skip declaring that.

```plain
let f = fun(x: Int) { x + 1; }
```
and Kima will still infer `f: (Int) -> Int`
